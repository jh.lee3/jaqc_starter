package com.jaqc.factoryauditmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaqcStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaqcStarterApplication.class, args);
	}

}
